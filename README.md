# LoginFormSample
よくあるログインフォームのサンプル。

Atom Editorで開発する場合は、以下プラグインをインストールする。

### 必須

|パッケージ名|説明|
|:--|:--|
|linter|ソースコード静的解析ツール。構文エラーやコードスタイル(括弧の後に必ずスペース入れる等)をチェックする。<br>リアルタイムに実行。(保存時にチェックさせたい場合は、Settingsから「Lint on Change」のチェックを外す。)<br>※他のサイトを見ると、Atomのコアパッケージ(whitespace)と競合してエラーになるとあるが、最新バージョンではエラーが解消されている模様。|
|linter-ui-default| Atom上にlinterでのチェック結果を表示するために必要。 |
|linter-htmlhint| HTML用のlinter。プロジェクト直下に`.htmlhintrc`があるとそっちの設定を読み込んでくれる。|
| linter-stylelint | スタイル用(CSS、SCSS、Less、PostCSS、SugarSS)のlinter。<br>プロジェクト直下に`.stylelintrc`があるとそっちの設定を読み込んでくれる。  |
| editorconfig | ファイル編集におけるルール(改行コードはLFにする、末尾に改行入れる等)を定義する。<br>プロジェクト直下に`.editorconfig`があるとそっちの設定を読み込んでくれる。<br>ファイル保存時に実行。 |
| atom-beautify | ソースコード自動整形ツール。<br><kbd>Ctrl</kbd>+<kbd>Alt</kbd>+<kbd>B</kbd>で実行。|


※JavaScript用のlinterはいつかいれる。たぶん。


### 任意
|パッケージ名|説明|
|:--|:--|
|pigments|カラーコード値が表示色でハイライトされて表示される。
|atom-html-preview|HTMLをAtom内の別タブでリアルタイムプレビュー。<br><kbd>Ctrl</kbd>+<kbd>Shift</kbd>+<kbd>H</kbd>で実行。
|autocomplete-paths|src属性等のファイルパスの入力を補完してくれる。|
|emmet| HTMLやCSSの記述が楽になる。使い方は他サイトを参照。|
